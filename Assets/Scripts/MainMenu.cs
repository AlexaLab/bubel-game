﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public int bubels;
    public int earnedBubels;
    public Text bubelsText;

    void Start()
    {
        bubels = PlayerPrefs.GetInt("Bubels");
        earnedBubels = PlayerPrefs.GetInt("Score");
        bubels += earnedBubels;
        PlayerPrefs.SetInt("Bubels", bubels);
        bubelsText.text = bubels.ToString();

        earnedBubels = 0;
        PlayerPrefs.SetInt("Score", earnedBubels);
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void InShop()
    {
        SceneManager.LoadScene(3);
    }
}
