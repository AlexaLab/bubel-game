﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Ball : MonoBehaviour
{
    public int score;
    [SerializeField] Text scoreText;
    public bool isX2;

    public void Start()
    {
        isX2 = PlayerPrefs.GetInt("IsX2") == 1 ? true : false;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Point")
        {
            Destroy(other.gameObject);
            if (isX2) score += 2;
            else score++;
        }

        if (other.gameObject.tag == "Let")
        {
            PlayerPrefs.SetInt("Score", score);
            SceneManager.LoadScene(2);
        }
    }
    void Update()
    {
        scoreText.text = score.ToString();
    }
}
