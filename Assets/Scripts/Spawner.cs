﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public Transform spawnPos;
    [SerializeField] Vector2 range;
    [SerializeField] GameObject let;
    [SerializeField] GameObject point;
    public AudioSource sound;

    void Start()
    {
        StartCoroutine(SpawnLet());
        StartCoroutine(SpawnPoint());
        sound.Play();
    }

    IEnumerator SpawnLet()
    {
        yield return new WaitForSeconds(1);
        Vector2 posLet = spawnPos.position + new Vector3(0, Random.Range(-range.y, range.y));
        Instantiate(let, posLet, Quaternion.identity);

        RepeatSpawnLet();
    }

    void RepeatSpawnLet()
    {
        StartCoroutine(SpawnLet());
    }


    IEnumerator SpawnPoint()
    {
        yield return new WaitForSeconds(2);
        Vector2 posPoint = spawnPos.position + new Vector3(0, Random.Range(-range.y, range.y));
        Instantiate(point, posPoint, Quaternion.identity);

        RepeatSpawnPoint();
    }

    void RepeatSpawnPoint()
    {
        StartCoroutine(SpawnPoint());
    }   
}
