﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LossScene : MonoBehaviour
{
    public int score;
    public Text scoreText;
    public bool isX2;

    public void Start()
    {
        score = PlayerPrefs.GetInt("Score");
        scoreText.text = score.ToString();

        isX2 = false;
        PlayerPrefs.SetInt("IsX2", isX2 ? 1 : 0);
    }

    public void ToMenu()
    {
        SceneManager.LoadScene(0);
    }

}
