﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    public int bubel;
    public Text bubelText;
    public bool isX2 = false;

    void Start()
    {
        bubel = PlayerPrefs.GetInt("Bubels");
        isX2 = PlayerPrefs.GetInt("IsX2") == 1 ? true : false;
    }

    void Update()
    {
        bubelText.text = bubel.ToString();
    }

    public void BuyX2()
    {
        if (bubel >= 10 && isX2 == false)
        {
            isX2 = true;
            bubel -= 10;

            PlayerPrefs.SetInt("Bubels", bubel);
            PlayerPrefs.SetInt("IsX2", isX2 ? 1 : 0);
        }
    }

    public void ExitShop()
    {
        SceneManager.LoadScene(0);
    }
}
