﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallRotation : MonoBehaviour
{
    [SerializeField] GameObject centerBubel;
    bool direction = false;

    void FixedUpdate()
    {
        if (direction == false)
            centerBubel.transform.Rotate(0, 0, 150 * Time.deltaTime);
        else
            centerBubel.transform.Rotate(0, 0, -150 * Time.deltaTime);
    }

    public void ChangeDirection()
    {
        direction = !direction;
    }
}
