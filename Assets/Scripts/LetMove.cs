﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetMove : MonoBehaviour
{
    [SerializeField] GameObject let;
    [SerializeField] float speed;

    void FixedUpdate()
    {
        let.transform.Translate(speed * Time.deltaTime, 0, 0);
    }
}
