﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointMove : MonoBehaviour
{
    [SerializeField] GameObject point;
    [SerializeField] float speed;
    public GameObject bubelEffect;

    void FixedUpdate()
    {
        point.transform.Translate(speed * Time.deltaTime, 0, 0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            Instantiate(bubelEffect, transform.position, Quaternion.identity);
        }
    }
}
